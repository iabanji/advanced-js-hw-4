async function addActors(li, characters) {
    let persons = [];

    for (let i in characters) {
        const data = await fetch(characters[i]).then(function (res) {
            return res.json();
        });
        persons.push(data.name);
    }

    const br = document.createElement('br');
    const b = document.createElement('b');

    b.append(persons.join(','));
    li.append(br, b);
}

window.addEventListener("load", (event) => {
    const but = document.getElementById('but');
    const root = document.getElementById('root');
    const animation = document.getElementById('animation');
    but.addEventListener('click', (e) => {
        e.preventDefault();
        root.textContent = '';
        animation.style.display = 'block';
        fetch('https://ajax.test-danit.com/api/swapi/films').then(function (res) {
            return res.json();
        }).then(function (films) {
            animation.style.display = 'none';
            films.forEach((film) => {
                let ul = document.createElement('ul');
                let li1 = document.createElement('li');

                li1.textContent = 'номер епізоду - ' + film.episodeId;
                ul.append(li1);
                let li2 = document.createElement('li');

                addActors(li2, film.characters);

                li2.textContent = 'назва фільму - ' + film.name;
                ul.append(li2);
                let li3 = document.createElement('li');

                li3.textContent = 'короткий зміст - ' + film.openingCrawl;
                ul.append(li3);

                root.append(ul);
            });
        }).catch(function (err) {
            animation.style.display = 'none';
            root.textContent = 'Error message - ' + err.message;
        });
    });
});