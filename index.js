const URL = "https://ajax.test-danit.com/api/swapi/films";

const but = document.getElementById('but');
const root = document.getElementById('root');
const animation = document.getElementById('animation');

class Film {
    request(url) {
        return fetch(url).then(function (res) {
            return res.json();
        });
    }

    getFilms(url) {
        return this.request(url);
    }

    getCharacters(characters) {
        const charactersRequests = characters.map((character) => {
            return this.request(character);
        });

        return Promise.allSettled([...charactersRequests]);
    }

    renderFilms(films) {
        let filmsList = document.createElement("ul");

        let filmItems = films.map(({ name: planetname, episodeId, openingCrawl, characters }, index) => {
            let filmItem = document.createElement('li');

            let ul = document.createElement('ul');
            let li1 = document.createElement('li');

            li1.textContent = 'номер епізоду - ' + episodeId;
            ul.append(li1);
            let li2 = document.createElement('li');

            li2.textContent = 'назва фільму - ' + planetname;
            ul.append(li2);
            let li3 = document.createElement('li');

            li3.textContent = 'короткий зміст - ' + openingCrawl;
            ul.append(li3);
            filmItem.append(ul);

            setTimeout(() => {
                this.getCharacters(characters).then((characters) => {

                    let characterItems = characters.map(({ value }) => {
                        return value.name;
                    });

                    const br = document.createElement('br');
                    const b = document.createElement('b');

                    b.append(characterItems.join(','));
                    li2.append(br, b);
                });
            }, 1000 * index);

            return filmItem;
        });

        filmsList.append(...filmItems);
        return filmsList;
    }
}

but.addEventListener('click', (e) => {
    e.preventDefault();
    root.textContent = '';
    animation.style.display = 'block';

    let film = new Film();

    film
        .getFilms(URL)
        .then((films) => {
            animation.style.display = 'none';
            root.append(film.renderFilms(films));
        })
        .catch((err) => {
            animation.style.display = 'none';
            root.textContent = 'Error message - ' + err.message;
            console.log(err);
        });
});
